let i = 0;
let cancel;
let timer = 3;
let mlSec = 1000;
let stopped = false;
let stopClock;

let buttonCancel = document.createElement('button');
let buttonRestore = document.createElement('button');
let clock = document.createElement('div');

let div = document.querySelector('.images-wrapper');
let img = document.querySelectorAll('.image-to-show');
inVisible();
buttonCancel.innerText = 'Припинити';
buttonRestore.innerText = 'Відновити показ';

document.body.append(buttonCancel);
document.body.append(buttonRestore);
div.after(clock);

function inVisible() {
    img.forEach((element) => {
        element.hidden = true;
    })
}
function moveCircle() {
    if (i > img.length - 1) {
        i = 0;
    };

}
timerPicture();
function timerPicture() {
    if (i > 0) {
        img[i - 1].hidden = true;
        img[i].hidden = false;
    } else {
        img[img.length - 1].hidden = true;
        img[i].hidden = false
    };
    i++;
    moveCircle();
    cancel = setTimeout(timerPicture, 3000);
}

buttonCancel.addEventListener('click', () => {
    clearTimeout(cancel);
    clearTimeout(stopClock);
    stopped = false;

});
buttonRestore.addEventListener('click', () => {
    if (!stopped) {
        timer = 3;
        setTimeout(timerPicture, 3000);
        timerClock();
    }
    stopped = true;
}
);
timerClock();
function timerClock() {
    if (timer == 0) {
        timer = 3;
    };
    clock.innerText = `Timer: ${timer}`;
    timer--;
    stopClock = setTimeout(timerClock, 1000);
};

